# La Paradoja del Cuidado

Consigue mantener El Sistema activo, 
descubrirás que quizás hace falta algo más que poner la maquinaria en marcha.

* Píldora interactiva educativa realizada para la asignatura Animación por Computador del Máster en Informática Gráfica de la Universidad Rey Juan Carlos (Madrid).

- Implementado con C# sobre el motor gráfico Unity.

[Desarrollado en 2019]

## Requisitos
Versión de Unity: 2017.4.29f1

## Controles
AWSD flechas: mover al personaje Morade
shift: esprintar
T: trabajar/cuidar
E: enseñar a cuidar
(todos ellos se deben mantener pulsados para que se produzca su efecto, hasta que, dado el caso, éste finalice)

El personaje: Morade puede trabajar para El Sistema sobre una plataforma sin reservar, 
cuidar a habitantes sin energía o enseñarlos a que participen de los cuidados.

## Créditos
Realizado por Clara Peñalva Carbonell [[webpage](https://clarapenyalva.com/)]

Los objetos han sido modelados, texturizados y animados a excepción del suelo del escenario,
del que se ha partido de una base de un objeto de la AssetStore sobre la que se ha modificado el modelo.
