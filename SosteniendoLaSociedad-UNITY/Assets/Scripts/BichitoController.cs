﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.AI;
using UnityEngine.UI;

public class BichitoController : MonoBehaviour
{

    public enum StateBichito { NoEnergy = 0, Energy = 1, Produce = 2, Learn = 3, Care = 4 };

    [Header("PARAMETERS BICHITO (sobre 100)")]
    public float deltaMoreEnergy = 50f;
    public float deltaLessEnergy = 8f; //antes 10 
    public float deltaKnow = 25f;

    [Header("General")]
    public StateBichito state = StateBichito.NoEnergy;
    public bool moving = false;
    public GameObject activator = null;

    [Header("Energy")]
    public float energy = 50f; //umbral: [0, 100]
    public float maxEnergy = 100f;

    [Header("Care")]
    public float know = 0.0f;
    public float maxKnow = 100f; 
    public bool knowCare = false;
    public bool otherBichoNeedCare = false;
    public BichitoController bichitoRef = null;

    [Header("Produce")]
    public PlatformController platformRef = null;

    [Header("Wander Settings")]
    public Vector3 objective;
    public bool hasObjective = false;
    public MeshRenderer floor;
    public float minDist = .5f;
    [Range(0f, 100f)]
    public float minMovDist = 3;
    [Range(0f, 100f)]
    public float maxMovDist = 9;

    [Header("Face")]
    [Header("Images")]
    public Image eyeRight;
    public Image eyeLeft;
    public Image mouth;

    [Header("Sprites")]
    public Sprite mouthAngry1;
    public Sprite mouthAngry2;
    public Sprite mouthHappy1;
    public Sprite mouthHappy2;

    public Sprite eyeNormal;
    public Sprite eyeTired;

    public SkinnedMeshRenderer m_Material;
    private Color greyColor;
    private Color iniColor;
    private Color femColor;

    Rigidbody m_Rigidbody;
    Animator m_Animator;
    NavMeshAgent m_NavMeshAgent;

    // Use this for initialization
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_NavMeshAgent = GetComponent<NavMeshAgent>();
        greyColor = new Color(0.4f, 0.4f, 0.4f, 0.65f);
        femColor = new Color(0.561f, 0.141f, 0.765f);
        iniColor = m_Material.material.color;
    }

    // Update is called once per frame
    void Update()
    {
        //se actualiza según el estado en el que esté
        Debug.Log("Bichito State: " + state);
        GetComponent<NavMeshAgent>().enabled = moving;
        if (knowCare)
        {
            deltaLessEnergy = 6f;
            deltaMoreEnergy = 60f;
        }
        else {
            deltaLessEnergy = 8f;
            deltaMoreEnergy = 50f;
        }



        switch (state)
        {
            case StateBichito.NoEnergy:
                Wander();

                m_NavMeshAgent.speed = 0.3f;
                break;

            case StateBichito.Energy:
                if (knowCare)
                {
                    GoToCare();
                    m_NavMeshAgent.speed = 0.9f;
                }
                else {
                    m_NavMeshAgent.speed = 0.7f;
                }
                if (!otherBichoNeedCare || !knowCare)
                    GoToProduce();
                
                break;

            case StateBichito.Produce:
                Produce();

                break;

            case StateBichito.Care:
                if (Care())
                    StopCare();

                break;

            case StateBichito.Learn: //passive state
                break;
        }

        UpdateAnimator();

        //interpolar color de ini a gris según la energía
        if (state != StateBichito.Learn)
            m_Material.material.color = new Color(Mathf.Lerp(greyColor.r, iniColor.r, energy / 100.0f),
                Mathf.Lerp(greyColor.g, iniColor.g, energy / 100.0f),
                Mathf.Lerp(greyColor.b, iniColor.b, energy / 100.0f), 0.64f);
    }

    void UpdateAnimator()
    {
        // update the animator parameters
        switch (state)
        {
            case StateBichito.NoEnergy:
                m_Animator.SetBool("Produce", false);
                m_Animator.SetBool("Care", false);
                m_Animator.SetBool("Learn", false);
                m_Animator.SetBool("Energy", false);
                m_Animator.SetBool("Moving", moving);

                mouth.sprite = mouthAngry1;
                eyeLeft.sprite = eyeTired;
                eyeRight.sprite = eyeTired;
                break;

            case StateBichito.Energy:
                m_Animator.SetBool("Produce", false);
                m_Animator.SetBool("Care", false);
                m_Animator.SetBool("Learn", false);
                m_Animator.SetBool("Energy", true);
                m_Animator.SetBool("Moving", moving);

                mouth.sprite = mouthHappy1;
                eyeLeft.sprite = eyeNormal;
                eyeRight.sprite = eyeNormal;
                break;

            case StateBichito.Produce:
                m_Animator.SetBool("Produce", true);
                m_Animator.SetBool("Care", false);
                m_Animator.SetBool("Learn", false);
                m_Animator.SetBool("Energy", true);
                m_Animator.SetBool("Moving", moving);

                if (energy > 60.0f)
                {
                    eyeLeft.sprite = eyeNormal;
                    eyeRight.sprite = eyeNormal;
                    mouth.sprite = mouthHappy1;
                }
                else if (energy > 30.0f)
                {
                    eyeLeft.sprite = eyeNormal;
                    eyeRight.sprite = eyeNormal;
                    mouth.sprite = mouthHappy2;
                }
                else
                {
                    eyeLeft.sprite = eyeTired;
                    eyeRight.sprite = eyeTired;
                    mouth.sprite = mouthAngry2;
                }
                break;

            case StateBichito.Care:
                m_Animator.SetBool("Produce", false);
                m_Animator.SetBool("Care", true);
                m_Animator.SetBool("Learn", false);
                m_Animator.SetBool("Energy", true);
                m_Animator.SetBool("Moving", moving);
                eyeLeft.sprite = eyeNormal;
                eyeRight.sprite = eyeNormal;
                mouth.sprite = mouthHappy2;
                break;

            case StateBichito.Learn: //passive state
                m_Animator.SetBool("Produce", false);
                m_Animator.SetBool("Care", false);
                m_Animator.SetBool("Learn", true);
                m_Animator.SetBool("Energy", true);
                m_Animator.SetBool("Moving", moving);
                eyeLeft.sprite = eyeNormal;
                eyeRight.sprite = eyeNormal;
                mouth.sprite = mouthHappy1;
                break;
        }
    }

    public void iniGame()
    {
        state = StateBichito.Energy;
        moving = true;
        activator = null;

        //care
        knowCare = false;
        otherBichoNeedCare = false;
        bichitoRef = null;

        //produce
        platformRef = null;
        //wander
        //objective;
        hasObjective = false;
    }

    //------------------- CARE --------------------

    public void GoToCare()
    {
        Vector3 projPos = Vector3.ProjectOnPlane(transform.position, Vector3.up);

        if (otherBichoNeedCare == false && moving == true)
        {
            //SEARCH OBJECTIVE
            int i = 0;
            while (i < BichitoManager.instance.bichitos.Count &&
            !otherBichoNeedCare)
            {
                if (BichitoManager.instance.bichitos[i].isFreeBeingCare(this.gameObject))
                {
                    //hasObjective = true;                    
                    otherBichoNeedCare = true;
                    platformRef = null;
                    activator = null;
                    moving = true;
                    bichitoRef = BichitoManager.instance.bichitos[i];

                    //SE CAMBIA LA REFERENCIA DEL OTRO
                    bichitoRef.activator = this.gameObject;
                    bichitoRef.moving = false; //OJO: se fija la posición para que espere al otro
                    objective = Vector3.ProjectOnPlane(bichitoRef.transform.position, Vector3.up);
                    GetComponent<NavMeshAgent>().SetDestination(objective);   
                }
                i++;
            }
        }
        else if (otherBichoNeedCare == true && Vector3.Distance(projPos, objective) <= minDist)
        {
            //hasObjective = false;
            otherBichoNeedCare = false;
            platformRef = null;
            activator = null;
            moving = false;
            this.state = StateBichito.Care;
        }
    }

    public bool Care()
    {
        bool done = false;
        //que el bicho esté en estado NoEnergy
        if (bichitoRef != null)
        {
            if (bichitoRef.isFreeBeingCare(this.gameObject))
            {
                done = bichitoRef.GainingEnergy(this.gameObject); //llamar a recuperar energía
                this.state = StateBichito.Care;
            }
        }
        else
        {
            ResetToWander();
        }

        return done;
    }

    public void StopCare()
    {
        this.state = StateBichito.Energy;
        this.moving = true;
        hasObjective = false;
        otherBichoNeedCare = false;
        platformRef = null;

        bichitoRef.StopBeingCare();
        bichitoRef = null;
    }
   
    //------------- CURES TO RECEIVE --------------

    public bool isFreeBeingCare(GameObject act)
    {
        if ((state == StateBichito.NoEnergy) && 
            (activator == null || activator == act))
            return true;
        else
            return false;
    }

    public bool GainingEnergy(GameObject act)
    {
        energy += deltaMoreEnergy * Time.deltaTime;

        if (energy < maxEnergy)
        {
            moving = false;
            activator = act;
            otherBichoNeedCare = false;
            bichitoRef = null;
            platformRef = null;
            hasObjective = false;

            state = StateBichito.NoEnergy;
            return false;
        }
        else
        {
            return true;
        }
    }

    public void StopBeingCare() {
        moving = true;
        activator = null;
        otherBichoNeedCare = false;
        bichitoRef = null;
        platformRef = null;
        hasObjective = false;
        if (energy >= maxEnergy)
            state = StateBichito.Energy;
        else
            state = StateBichito.NoEnergy;
    }

    //------------------- LEARN -------------------

    public bool isFreeLearning(GameObject act)
    {
        if (!knowCare &&
            (state != StateBichito.Care && state != StateBichito.NoEnergy) &&
            (activator == act || activator == null))
            return true;
        else
            return false;
    }

    public bool Learn(GameObject act)
    {
        know += deltaKnow * Time.deltaTime;
        m_Material.material.color = new Color(Mathf.Lerp(iniColor.r, femColor.r, know / 100.0f),
                Mathf.Lerp(iniColor.g, femColor.g, know / 100.0f),
                Mathf.Lerp(iniColor.b, femColor.b, know / 100.0f), 0.64f);

        //m_Material.material.color =  femColor;
        if (know < maxKnow)
        {
            moving = false;
            activator = act;
            otherBichoNeedCare = false;
            bichitoRef = null;
            platformRef = null;
            hasObjective = false;

            state = StateBichito.Learn;
            return false;
        }
        else
        {
            return true;
        }

    }

    public void StopLearning()
    {
        moving = true;
        activator = null;
        otherBichoNeedCare = false;
        bichitoRef = null;
        platformRef = null;
        hasObjective = false;
        m_Material.material.color = iniColor;
        state = StateBichito.Energy;

        if (know >= maxKnow)
        {
            knowCare = true;
            iniColor = femColor;
        }
        else
        {
            knowCare = false;
            know = 0.0f;
        }
    }

    //------------------ PRODUCE ------------------

    public void GoToProduce()
    {
        Vector3 projPos = Vector3.ProjectOnPlane(transform.position, Vector3.up);

        if (hasObjective == false && moving == true)
        {
            //SEARCH OBJECTIVE
            int i = 0;
            while (i < PlatformManager.instance.platforms.Count &&
                !hasObjective)
            {
                if (PlatformManager.instance.platforms[i].isFree(this.gameObject))
                {                    
                    moving = true;
                    activator = null;
                    //otherBichoNeedCare = false;
                    bichitoRef = null;
                    platformRef = PlatformManager.instance.platforms[i];
                    hasObjective = true;

                    platformRef.activator = this.gameObject;
                    objective = Vector3.ProjectOnPlane(platformRef.transform.position, Vector3.up);
                    GetComponent<NavMeshAgent>().SetDestination(objective);
                }
                i++;
            }
        }
        else if (hasObjective == true &&  Vector3.Distance(projPos, objective) <= minDist)
        {
            moving = false;
            activator = null;
            //otherBichoNeedCare = false;
            bichitoRef = null;              
            hasObjective = false;
            state = StateBichito.Produce;
        }
    }

    public void Produce()
    {
        if (platformRef != null)
        {
            //que la plataforma esté con isActive == false
            if (platformRef.isFree(this.gameObject) && energy > 0.0f)
            {
                this.state = StateBichito.Produce;
                moving = false;
                activator = null;
                otherBichoNeedCare = false;
                bichitoRef = null;
                hasObjective = false;
                platformRef.Activate(this.gameObject); //llamar a hacer luz

                //lose energy
                energy -= deltaLessEnergy * Time.deltaTime;
            }
            else
            {
                ResetToWander();
            }

            if (energy <= 0.0f)
            {
                StopProduce();
            }
        }
        else {
            ResetToWander();
        }

    }



    public void StopProduce()
    {
        platformRef.Deactivate();
        platformRef = null;
        moving = true;
        activator = null;
        otherBichoNeedCare = false;
        bichitoRef = null;
        hasObjective = false;
        this.state = StateBichito.NoEnergy;
        energy = 0.0f;
    }


    //------------------ MOVING ------------------

    public void ResetToWander()
    {
        if (energy > 0.0f)
        {
            state = StateBichito.Energy;
            moving = true;
            activator = null;
            otherBichoNeedCare = false;
            bichitoRef = null;
            platformRef = null;
            hasObjective = false;
        }
        else
        {
            state = StateBichito.NoEnergy;
            moving = true;
            activator = null;
            otherBichoNeedCare = false;
            bichitoRef = null;
            platformRef = null;
            hasObjective = false;
        }
    }

    public void Wander()
    {
        Vector3 projPos = Vector3.ProjectOnPlane(transform.position, Vector3.up);

        if (hasObjective == false && moving == true && activator == null)
        { 
            Vector2 direction = UnityEngine.Random.insideUnitCircle.normalized;
            float magnitude = UnityEngine.Random.Range(minMovDist, maxMovDist);
            //Vector3 sampledPoint = direction * magnitude + transform.position;
            Vector3 sampledPoint = new Vector3(direction.x, 0, direction.y).normalized * magnitude + transform.position;
            NavMeshHit cosa;
            NavMesh.SamplePosition(sampledPoint, out cosa, float.PositiveInfinity, NavMesh.AllAreas);
            objective = Vector3.ProjectOnPlane(cosa.position, Vector3.up);
            GetComponent<NavMeshAgent>().SetDestination(cosa.position);

            moving = true;
            activator = null;
            otherBichoNeedCare = false;
            bichitoRef = null;
            platformRef = null;
            hasObjective = true;
        }
        else if (hasObjective == true && Vector3.Distance(projPos, objective) <= minDist)
        {
            hasObjective = false;
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, minMovDist);
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, maxMovDist);

        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(objective, 1);

    }
}