﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BichitoManager : MonoBehaviour {

    public List<BichitoController> bichitos;

    public static BichitoManager instance;

    private void Awake()
    {
        if (instance != null && instance != this)
            Destroy(gameObject);
        else
            instance = this;
    }

    // Update is called once per frame
    void Update () {
		
	}
}
