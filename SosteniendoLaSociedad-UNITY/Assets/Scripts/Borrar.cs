﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Borrar : MonoBehaviour
{ 
    public Transform myCamera;
    public Text healthText;
    public float playerSpeed = 6.0f;
    public GameObject interfFinal2;

    public int iniHealth = 10;
    public bool isProtected;
    public float maxTimeProtected = 2.0f;

    private float speedY;
    private Vector3 cameraOffset;
    private float moveX, moveZ;
    private Vector3 movePos;
    private Vector3 lookVector;

    private int currentHealth;
    private int timeProtected;
    private Material material;
    //private Timer timerColor;
    // Use this for initialization
    void Start()
    {
        interfFinal2 = GameObject.FindWithTag("FinalInterf2");
        interfFinal2.SetActive(false);

        myCamera = GameObject.FindWithTag("MainCamera").transform;
        cameraOffset = myCamera.position - transform.position;

        healthText = GameObject.FindWithTag("TextLifePlayer").GetComponent<UnityEngine.UI.Text>();

        if (iniHealth <= 0)
            iniHealth = 10;
        currentHealth = iniHealth;
        healthText.text = "Vida: " + currentHealth + " / " + iniHealth;

        material = this.gameObject.GetComponent<Renderer>().material;
        if(maxTimeProtected <= 0.0f)
            maxTimeProtected = 4.0f;

        //timerColor = new Timer(0.0f);
    }

    // Update is called once per frame
    void Update()
    {
        //movimiento del personaje
        moveX = Input.GetAxis("Horizontal") * playerSpeed;
        moveZ = Input.GetAxis("Vertical") * playerSpeed;

        movePos = new Vector3(moveX, 0.0f, moveZ) * Time.deltaTime;
        lookVector = new Vector3(moveX, 0.0f, moveZ);

        transform.LookAt(transform.position + lookVector);
        myCamera.position = transform.position + cameraOffset;

        this.transform.position += movePos;

        if (isProtected)
        {
            //if (timerColor.updateTimer())//ya ha alcanzado el máximo
            //{ 
            //    isProtected = false;
            //    material.color = iniColor;
            //}
        }       

        if (currentHealth <= 0)
        {
            //MUERTO
            interfFinal2.SetActive(true);
            Time.timeScale = 0.0f;
        }

    }

    //Otro agente le ayuda
    public void ActivateProtection()
    {
        isProtected = true;
        material.color = new Color(0.83f, 0.77f, 0.83f);
        //timerColor = new Timer(maxTimeProtected);
    }

    //Enemigo le daña
    public void DamagePlayer(int amountDamage)
    {
        if (!isProtected)
            currentHealth -= amountDamage;
        healthText.text = "Vida: " + currentHealth + " / " + iniHealth;
    }
}
