﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]

public class MoradeController : MonoBehaviour
{

    [SerializeField] float m_MovingTurnSpeed = 360;
    [SerializeField] float m_StationaryTurnSpeed = 180;
    [SerializeField] float m_MoveSpeedMultiplier = 1f;
    public float m_ForwardAmount;
    public bool m_Moving;
    public bool m_RunMorade;

    public enum StateMorade { Wander = 0, Produce = 1, Care = 2, Teach = 3 };
    public StateMorade state = StateMorade.Wander;

    private Vector3 m_Move;
    Rigidbody m_Rigidbody;
    Animator m_Animator;
    float m_TurnAmount;
    Vector3 m_GroundNormal;
    CapsuleCollider m_Capsule;

    public BichitoController bichitoRef = null;
    public PlatformController platformRef = null;
    // Use this for initialization
    void Start()
    {
        m_Animator = GetComponent<Animator>();
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Capsule = GetComponent<CapsuleCollider>();

        m_Rigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

    }

    private void Update()
    {
        Debug.Log("Morade State: " + state);

        if (state == StateMorade.Wander || state == StateMorade.Produce || state == StateMorade.Teach) // allow to move if isn't with bichito
        {
            // read inputs move
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");

            if (Input.GetKey(KeyCode.LeftShift) ||
                 Input.GetKey(KeyCode.RightShift))
                m_RunMorade = true;
            else
                m_RunMorade = false;
            // we use world-relative directions in the case of no main camera
            m_Move = v * Vector3.forward + h * Vector3.right;

            if (m_Move.magnitude > 0.001f)
                m_Moving = true;
            else
                m_Moving = false;
            Move();
        }
        else
        {
            m_Rigidbody.velocity = new Vector3();
            m_ForwardAmount = 0.0f;
            m_Move = new Vector3();
            m_Moving = false;
        }



        // send input and other state parameters to the animator
        UpdateAnimator();
    }

    //------------------- MOVE --------------------

    public void Move()
    {
        // convert the world relative moveInput vector into a local-relative
        // turn amount and forward amount required to head in the desired
        // direction.

        Vector3 move = m_Move;

        if (move.magnitude > 1f) move.Normalize();
        move = transform.InverseTransformDirection(move);

        move = Vector3.ProjectOnPlane(move, m_GroundNormal);
        m_TurnAmount = Mathf.Atan2(move.x, move.z);
        m_ForwardAmount = move.z;

        ApplyExtraTurnRotation();

        //Actualiza la posición de Morade
        //TODO interpolar corrector velocidad
        if (m_RunMorade)
            m_MoveSpeedMultiplier = 2.5f;
        else
            m_MoveSpeedMultiplier = 1.0f;

        Vector3 velocity = m_Move * m_MoveSpeedMultiplier;
        // we preserve the existing y part of the current velocity.
        velocity.y = m_Rigidbody.velocity.y;
        m_Rigidbody.velocity = velocity;


    }

    void UpdateAnimator()
    {
        // update the animator parameters
        m_Animator.SetFloat("Velocity", m_ForwardAmount, 0.1f, Time.deltaTime);
        m_Animator.SetBool("Run", m_RunMorade);

        // the anim speed multiplier allows the overall speed of walking/running to be tweaked in the inspector,
        // which affects the movement speed because of the root motion.
        m_Animator.SetBool("Moving", m_Moving);

        //TODO: estado cuidando

        //TODO: estado enseñando
        if (state == StateMorade.Teach)
        {
            m_Animator.SetBool("Teach", true);
        }
        else
            m_Animator.SetBool("Teach", false);

        if (state == StateMorade.Care)
        {
            m_Animator.SetBool("Care", true);
        }
        else
            m_Animator.SetBool("Care", false);

    }

    void ApplyExtraTurnRotation()
    {
        // help the character turn faster (this is in addition to root rotation in the animation)
        float turnSpeed = Mathf.Lerp(m_StationaryTurnSpeed, m_MovingTurnSpeed, m_ForwardAmount);
        transform.Rotate(0, m_TurnAmount * turnSpeed * Time.deltaTime, 0);
    }

    //------------------- TEACH -------------------

    public bool Teach(BichitoController bichito)
    {
        bool done = false;
        //mirar si el bicho está en estado Energy | Produce
        if (bichito.isFreeLearning(this.gameObject))
        {      
            this.state = StateMorade.Teach;
            this.bichitoRef = bichito;
            done = bichito.Learn(this.gameObject); //llamar a aprender de bicho      
        }
        return done;
    }

    public void StopTeach()
    {
        this.state = StateMorade.Wander;
        this.bichitoRef.StopLearning();
        this.bichitoRef = null;
    }

    //------------------- CARE --------------------

    public bool Care(BichitoController bichito)
    {
        bool done = false;
        //que el bicho esté en estado NoEnergy y sin otro asignado que vaya a por él
        if (bichito.isFreeBeingCare(this.gameObject))
        {
            this.state = StateMorade.Care;
            this.bichitoRef = bichito;
            done = bichito.GainingEnergy(this.gameObject); //llamar a recuperar energía            
        }
        return done;
    }

    public void StopCare()
    {
        this.state = StateMorade.Wander;
        this.bichitoRef.StopBeingCare();
        this.bichitoRef = null;
    }

    //------------------ PRODUCE ------------------

    public void Produce(PlatformController platform)
    {
        //que la plataforma esté con isActive == false
        if ((platform.isFree(this.gameObject)))
        {
            this.state = StateMorade.Produce;
            platformRef = platform;
            platform.Activate(this.gameObject); //llamar a recuperar energía
        }
    }

    public void StopProduce()
    {
        platformRef.Deactivate();
        platformRef = null;
        this.state = StateMorade.Wander;
    }

    //---------- DETECTION OF CHANGES -------------

    public void OnTriggerStay(Collider other)
    {
        Debug.Log("OnTriggerStay");

        if (other.tag == "Bichito" && state!= StateMorade.Produce)
        {
            Debug.Log("MoradeController - Stay - Bichito");
            BichitoController bichi = other.gameObject.GetComponent<BichitoController>();

            if (Input.GetButton("Trabajar") &&
                (bichitoRef == null || bichitoRef.gameObject == bichi.gameObject)) //CUIDAR
            {
                if (Care(bichi))
                    StopCare();
            }
            if (Input.GetButtonUp("Trabajar") && state == StateMorade.Care)
            {
                StopCare();
            }

            if (Input.GetButton("Ensenyar") && state != StateMorade.Care) //ENSEÑAR
            {
                if (Teach(bichi))
                    StopTeach();
            }
            if (Input.GetButtonUp("Ensenyar") && state == StateMorade.Teach)
            {
                StopTeach();
            }
        }
        else if ((other.tag == "Platform") &&
            !(state == StateMorade.Care || state == StateMorade.Teach))
        {
            Debug.Log("MoradeController - Stay - Platform");
            PlatformController platform = other.gameObject.GetComponent<PlatformController>();

            if (Input.GetButton("Trabajar")) //ILUMINAR
            {
                Produce(platform);
            }
            if (Input.GetButtonUp("Trabajar") && state == StateMorade.Produce)
            {
                StopProduce();
            }
        }
    }

    public void OnTriggerExit(Collider other)
    {
        Debug.Log("OnTriggerExit");
        if (other.tag == "Platform") //se sale de la plataforma con la tecla pulsada
        {
            Debug.Log("MoradeController - Exit - Platform");
            PlatformController platform = other.gameObject.GetComponent<PlatformController>();

            if (state == StateMorade.Produce && platformRef == platform)
            {
                StopProduce();
            }
        }

        if (other.tag == "Bichito") //se sale de la plataforma con la tecla pulsada
        {
            Debug.Log("MoradeController - Exit - Bichito");
            BichitoController bichi = other.gameObject.GetComponent<BichitoController>();

            if (state == StateMorade.Care && bichitoRef == bichi)
            {
                StopCare();
            }
            if (state == StateMorade.Teach && bichitoRef == bichi)
            {
                StopTeach();
            }
        }
    }
}
    //public void OnTriggerExit(Collider other)
    //{
    //if (other.tag == "Bichito")
    //{
    //    Debug.Log("MoradeController - Exit - Bichito");
    //    if (this.state == StateMorade.Care)
    //    { //si estaba cuidando y no ha acabado
    //        this.state = StateMorade.Wander;
    //        other.gameObject.GetComponent<BichitoController>().moving = true;
    //    }
    //    else if (this.state == StateMorade.Teach)
    //    {
    //        BichitoController bichi = other.gameObject.GetComponent<BichitoController>();

    //        //si ha dejado de aprender a medias, vuelve a 0
    //        if (bichi.state == BichitoController.StateBichito.Learn)
    //        {
    //            bichi.know = 0.0f;
    //            bichi.moving = true;
    //            bichi.state = BichitoController.StateBichito.Energy;
    //        }
    //    }
    //}
    //else if (other.tag == "Platform")
    //{
    //    Debug.Log("MoradeController - Exit - Platform");
    //    if (other.gameObject.GetComponent<PlatformController>().isFree(this.gameObject))
    //        other.gameObject.GetComponent<PlatformController>().Deactivate();
    //}
    //}

