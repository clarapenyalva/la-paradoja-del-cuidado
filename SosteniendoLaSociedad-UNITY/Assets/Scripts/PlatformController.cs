﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformController : MonoBehaviour {

    public bool isActive;
    public LightController light;

    public GameObject activator;
    private Material colorPlatformRef;
    private Color colorPlatformIni;
    private Color colorPlatformAct = new Color(0.16f, 0.16f, 0.07f);
    // Use this for initialization
    void Start () {
        colorPlatformRef = gameObject.GetComponent<MeshRenderer>().materials[1];
        colorPlatformIni = colorPlatformRef.GetColor("_EmissionColor");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public bool isFree(GameObject act)
    {
        if (activator == null || activator == act)
            return true;
        else
            return false;
    }


    public void Activate(GameObject act)
    {
        isActive = true;
        colorPlatformRef.SetColor("_EmissionColor", colorPlatformAct);
        activator = act;
        //se ilumina o cambia como sea el estado visual
        light.moreLight();
    }

    public void Deactivate()
    {
        isActive = false;
        colorPlatformRef.SetColor("_EmissionColor", colorPlatformIni);
        activator = null;
    }    

}
