﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LightController : MonoBehaviour {


    [Header("Sun Intensity")]
    public float intensity = 0f; //range
    public float maxIntensity = 30f; //TODO: al principi era 20
    public Slider slider;
    private float intensitySlider = 0f;

    [Header("PARAMETERS SUN (sobre 100)")]
    public float deltaMoreIntensity = 5.0f;
    public float deltaLessIntensity = 10.0f;

    [Header("Ambient Range")]
    public Light directionalLight;
    public float deltaIntenDirecL = 0.01f;
    public float maxIntensDirecL = 0.35f;
    public float minIntensDirecL = 0.35f;

    [Header("EndGame")]
    public Transform endGamePanel;

    private Animator animator;
    private Light light;

    // Use this for initialization
    void Start () {
        light = GetComponent<Light>();
        animator = GetComponent<Animator>();
        slider.value = intensitySlider;
    }
	
	// Update is called once per frame
	void Update () {
        //actualiza el nivel de luz según el valor de intensity
        lessLight();
        animator.SetFloat("speed", intensity / maxIntensity);
        slider.value = intensitySlider;
        Debug.Log("INTENSITYYYYYYYYYYYYYYY: "+ intensity);
        if (intensity >= maxIntensity-0.1f)
            EndGame();
    }

    private void EndGame()
    {
        endGamePanel.gameObject.SetActive(true);
        Time.timeScale = 0;
    }

    public void moreLight()
    {
        intensitySlider = Mathf.Clamp(intensitySlider + deltaMoreIntensity * Time.deltaTime, 0.0f, 100.0f);
        intensity = intensitySlider * maxIntensity / 100f;
        this.light.range = Mathf.Clamp(intensity, 0.0f, maxIntensity);

        //directionalLight.intensity = Mathf.Clamp(directionalLight.intensity + deltaIntenDirecL * Time.deltaTime, 
        //    minIntensDirecL, maxIntensDirecL);
    }

    public void lessLight()
    {
        intensitySlider = Mathf.Clamp(intensitySlider - deltaLessIntensity * Time.deltaTime, 0.0f, 100.0f);
        intensity = intensitySlider * maxIntensity / 100f;
        this.light.range = Mathf.Clamp(intensity, 0.0f, maxIntensity);

        //directionalLight.intensity = Mathf.Clamp(directionalLight.intensity - deltaIntenDirecL * Time.deltaTime,
        //    minIntensDirecL, maxIntensDirecL);
    }


}
